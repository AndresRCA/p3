list P = 16F877A
include <P16F877A.inc>

D0 EQU 20h
D1 EQU 21h
D2 EQU 22h
isStopped EQU 23h ; cuando es 0 es falso, 1 es verdadero (en el bit 0)
isStarting EQU 24h ; cuando es 0 el conteo no ha comenzado, 1 es que ya empezo (en el bit 0)
counter EQU 25h ; verifico si han pasado 0.5s o 1s
cociente EQU 26h
AUX EQU 27h

org 0h
        goto Fig
org 4h
;*****************************************
		btfss PIR1, 6
		goto RB
		call ADInt
		retfie
RB		movlw d'7'
		btfss INTCON, 1 ; bandera de RB0
		addwf PCL, 1 ; si la bandera es 0 ira a call TMRInt
		btfsc isStarting, 0
		goto Next
		bsf isStarting, 0
		bcf INTCON, 1 ; bajo la bandera de RB0
		retfie
Next    call RB0Int ; si la bandera es 1 ira aqui
		retfie
		call TMRInt
        retfie
;*****************************************
org 40h
;*********** Configuracion ***************
Fig    	clrf D0
        clrf D1
        clrf D2
		clrf isStopped
		clrf isStarting
		clrf counter
		clrf cociente
        bsf STATUS, 5
        clrf TRISB
        bsf TRISB, 0
        clrf TRISD
        bsf TRISD, 2 ; pin del pulsador de reset
        bsf INTCON, 7 ; global interrupt
        bsf INTCON, 4 ; external rb0 interrupt
		bsf INTCON, 6 ; habilito interrupciones en perifericos
		bsf PIE1, 0 ; habilito la interrupcion del overflow del TMR1
		bsf PIE1, 6 ; habilito la interrupcion del conversor A/D
		; el canal por defecto es AN0, y es el que se va a utilizar, el pin de master clear que esta arriba de AN0 servira como la fuente de 5V del potenciometro mientras que AN1 que esta debajo sera la tierra
        ; la velocidad de conversion sera Fosc/2, el cual es por defecto
		; por defecto el resultado ya esta justificado a la izquierda
		bcf OPTION_REG, 5 ;el timer cambia como temporizador. Su preescaler ya es 256
        bcf STATUS, 5
		bsf ADCON0, 0 ; enciendo el modulo conversor
		; configuro aqui el prescaler de T1CON (bits 5-4 => 11 = 1:8, 10 = 1:4, 01 = 1:2, 00 = 1:1)
		bsf T1CON, 5
		bsf T1CON, 4
		bcf T1CON, 1 ; el timer contara con el reloj interno
		bsf T1CON, 3 ; habilito el oscilador
		clrf TMR1H ; borro algun valor por si acaso (aunque se supone que lo estoy prendiendo el paso anterior)
		clrf TMR1L ; tal vez no sea necesario hacer estos clrf
        movlw d'255'
        movwf PORTD ;por si la salida tiene valores no esperados
;*****************************************
Espera	clrwdt
		btfsc isStarting, 0
		goto Go
		call display
		goto Espera
Go		bsf T1CON, 0 ; prendo el TMR1 (compuesto de dos registros, TMR1H:TMR1L)
        call MedSeg
		
Main	call display ; muestra el valor del display hasta que ocurra una interrupcion
		btfss PORTD, 2 ; pregunto si el reset esta en 1
        goto Main
		btfss isStopped, 0 ; si PORTD,2 esta en 1 y tambien esta detenido borro los valores y activo el conversor
		goto Main
		clrf D0 ; isStopped = 1 ya implica que el TMR1 esta desactivado
		clrf D1
		clrf D2
		call display ; muestro el valor de 0 0 0 por 600ms (contando el de la linea de abajo, call display dura 300ms)
;****** Estado de conversion (isStopped, 0 = 1  && PORTD, 2 = 1 (previamente)) = true ***********
Convert	call display ; en algun punto de este call display, la conversion activara una interrupcion
		btfss isStopped, 0 ; antes de activar el conversor pregunto si sigue detenido, si ya no esta detenido se va al Main donde el programa solo se encarga de mostrar el numero y el tmr1 esta activado
		goto Main
		bsf ADCON0, 2 ; tengo una duda aunque realmente no importa en este caso, que sucede si llamo bsf ADCON0, 2 antes de que la conversion anterior se termine? sigue la conversion anterior o genera una nueva?
		goto Convert
;************************************************************************************************

;*****************
;le doy el valor necesario para que interrumpa en medio segundo
MedSeg	movlw b'11011100'
		movwf TMR1L
		movlw b'00001011'
		movwf TMR1H ; el numero completo
		return
;*****************

;********Funciones que ocurren durante 1 segundo*******
;*****************
;retardo de 50ms usando el TMR0    
T0      bcf INTCON, 2 ;apago la bandera del tmr0 por si acaso
        movlw d'61'
        movwf TMR0
Back    clrwdt
		btfss INTCON, 2
        goto Back
        return
;*****************

;*****************
;retorna el valor a mostrar
tabla   addwf PCL, 1
        retlw b'01111110'
        retlw b'00001100'
        retlw b'10110110'
        retlw b'10011110'
        retlw b'11001100'
        retlw b'11011010'
        retlw b'11111010'
        retlw b'00001110'
        retlw b'11111110'
        retlw b'11001110'
;*****************

;*****************
;muestra los numeros en los displays
display clrwdt
		movf D0, 0
        call tabla
        movwf PORTB
        bcf PORTD, 5
        ;*******TMR0******
        call T0
        call T0
        ;*****************
        bsf PORTD, 5
        movf D1,0
        call tabla
        movwf PORTB
        bcf PORTD, 6
        ;*******TMR0******
        call T0
        call T0
        ;*****************
        bsf PORTD, 6
        movf D2,0
        call tabla
        movwf PORTB
        bcf PORTD, 7
        ;*******TMR0******
        call T0
        call T0
        ;*****************
        bsf PORTD, 7
        return
;*****************

;*************************************************************
;*****************
; rutina que se usa al llamar la interrupcion del TMR1 		
TMRInt	bcf PIR1, 0 ; apago la bandera del TMR1 overflow
		incf counter, 1 ; count varia entre 1 (01) y 2 (10) 
		btfsc counter, 1
		goto Pass ; count = 2, debo incrementar el segundo
		call MedSeg ; si no es (10) entonces espero el otro medio segundo
		return
Pass	clrf counter ; aqui ya ha pasado 1 segundo
		movlw d'9' ; esto es un 9 con respecto al 7 segmentos
		subwf D0, 0
		btfss STATUS, 2
			goto IncD0 ; al final de este goto hay un goto Final
		clrf D0
		movlw d'9'
		subwf D1, 0
		btfss STATUS, 2
			goto IncD1
		clrf D1
		movlw d'9'
		subwf D2, 0
		btfss STATUS, 2
			goto IncD2
		clrf D2
Final	call MedSeg
		return
		
IncD0	incf D0, 1
		goto Final
		
IncD1	incf D1, 1
		goto Final

IncD2	incf D2, 1
		goto Final
;*****************

;*****************
; rutina que se usa al llamar la interrupcion del RB0 		
RB0Int	bcf INTCON, 1 ; apago la bandera de RB0
		btfss isStopped, 0
		goto Stop
		bcf isStopped, 0 ; cuando lo resumo lo pongo en 0
		bsf T1CON, 0 ; prendo el TMR1
		call MedSeg
		return
Stop	bsf isStopped, 0 ; cuando lo paro lo pongo en 1
		bcf T1CON, 0 ; apago el TMR1
		return
;*****************

;*****************
; rutina que se usa al llamar la interrupcion del A/D		
ADInt	movwf AUX
		bcf PIR1, 6 ; apago la bandera de A/D
		;Algoritmo para dividir y extraer los digitos de ADRESH va aqui
		movf ADRESH, 0
		movwf D0
		movlw d'10'
Divide1	subwf D0, 1 ; resto, ej: 231 - 10
		btfsc STATUS, 0 ; si el carry es 0 entonces la resta dio negativa
		goto Div1 ; incremento cociente y sigo la resta
		addwf D0, 1 ; como la resta anterior dio negativa le sumo lo que le reste antes y eso me da el residuo de la division, es decir el digito que se pide
		movf cociente, 0
		movwf D1 ; ahora seria 23/10, 23 siendo el cociente anterior
		clrf cociente ; empiezo de nuevo con un fresco cociente = 0
		movlw d'10'
Divide2	subwf D1, 1 ; repito lo mismo de Divide1 pero para D1
		btfsc STATUS, 0
		goto Div2
		addwf D1, 1
		movf cociente, 0
		movwf D2 ; el cociente de la ultima operacion siempre va a ser el ultimo digito, porque la operacion en la tercera division en este caso seria 2/10
		clrf cociente ; limpio el cociente para la siguiente interrupcion
		movf AUX,0
		return

Div1	incf cociente, 1 ; incremento el valor del cociente por cada resta
		goto Divide1

Div2	incf cociente, 1 ; incremento el valor del cociente por cada resta
		goto Divide2
;*****************
end
